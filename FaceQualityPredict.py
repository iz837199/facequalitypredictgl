"""
This program trains models based on OFIQ quality measures to predict if the facial recognition software
will correctly identify an individual. This model can then be used to filter frames used for identification
in a live situation.
"""

import sys
import os
import csv
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split, GridSearchCV, learning_curve
from sklearn.linear_model import LogisticRegression
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.svm import SVC
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.metrics import precision_score, confusion_matrix
import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt

# initialize a dataframe to store all similarities and quality scores
aggregated_metrics = pd.DataFrame()

# loop through all cameras
for camera in ['camera_1', 'camera_2', 'camera_3']:
        
    # loop through all datasets
    for i in range(1, 20):
        dataset_ID = str(i).zfill(2)
        
        # define the folder locations
        faces_folder = os.path.join('C:\\Users\\David_UoR\\OneDrive - University of Reading\\CSMPR21_data\\captured_faces\\'+ camera, dataset_ID)    
    
        # read in the quality scores into a dataframe
        quality_scores = pd.read_csv(faces_folder+"\quality_scores.csv")
        # add to current quality scores with the overall dataframe
        aggregated_metrics = pd.concat([aggregated_metrics, quality_scores], ignore_index=True)

# if evaluation metrics folder does not exist create it
evaluation_metrics_folder = 'C:\\Users\\David_UoR\\OneDrive - University of Reading\\CSMPR21_data\\evaluation_metrics\\'
os.makedirs(evaluation_metrics_folder, exist_ok=True)

# write aggregated metrics to csv
aggregated_metrics_filepath = os.path.join(evaluation_metrics_folder, 'aggregated_metrics.csv')
aggregated_metrics.to_csv(aggregated_metrics_filepath, index=False)

print("Quality scores aggregation complete")

# data cleaning

# delete rows where all quality scores are 0
quality_columns = aggregated_metrics.columns[5:]
aggregated_metrics[quality_columns] = aggregated_metrics[quality_columns].apply(pd.to_numeric, errors='coerce')
filtered_aggregated_metrics = aggregated_metrics[(aggregated_metrics[quality_columns] != 0).any(axis=1)]
print(filtered_aggregated_metrics)

# print rows with any missing values
rows_with_missing_values = filtered_aggregated_metrics[filtered_aggregated_metrics.isnull().any(axis=1)]
print("Rows with missing values:")
print(rows_with_missing_values)

# plot the distributions of each column in the quality_columns of the filtered_aggregated_metrics DataFrame
def plot_distributions(df, columns):
    # Set up the plotting area
    num_columns = len(columns)
    num_rows = (num_columns + 2) // 3  # 3 plots per row
    fig, axes = plt.subplots(num_rows, 3, figsize=(15, num_rows * 5))
    
    # flatten the axes array for easy iteration
    axes = axes.flatten()
    
    # plot each column
    for i, col in enumerate(columns):
        df[col].plot(kind='hist', bins=30, ax=axes[i], title=col)
        axes[i].set_xlabel(col)
        axes[i].set_ylabel('Frequency')
    
    # Remove any unused subplots
    for j in range(i + 1, len(axes)):
        fig.delaxes(axes[j])
    
    # Adjust layout
    plt.tight_layout()
    plt.show()

# define the quality columns
quality_columns = filtered_aggregated_metrics.columns[5:]

# call the function to plot distributions
plot_distributions(filtered_aggregated_metrics, quality_columns)

# impute missing values with the mean of each quality column using .loc
filtered_aggregated_metrics.loc[:, quality_columns] = filtered_aggregated_metrics[quality_columns].apply(lambda col: col.fillna(col.mean()))

# assign the cleaned data to a new variable
clean_aggregated_metrics = filtered_aggregated_metrics

# write cleaned aggregated metrics to csv
aggregated_metrics_filepath = os.path.join(evaluation_metrics_folder, 'clean_aggregated_metrics.csv')
clean_aggregated_metrics.to_csv(aggregated_metrics_filepath, index=False)


print("Cleaning data complete")
aggregated_metrics = clean_aggregated_metrics

### use the quality metrics in the filtered agregated_metrics dataframe to predict a correct identification

# extract the relevent features and target columns
# X_raw = all_quality_scores.drop(["Similarity Score", "Match", "Filename", "Unified Quality Score", "Identification", "Face", "Under Exposure Prevention", "Over Exposure Prevention"], axis=1) # features
X = aggregated_metrics.drop(["Match"], axis=1) # features
y = aggregated_metrics['Match'] # targets

# split the data into training and test sets
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=12)

# store X_test filenames and similarities
filenames_similarities = X_test[['Filename', 'Similarity Score']].copy()

# rename columns to remove spaces
filenames_similarities.columns = ['Filename', 'Similarity_Score']

# drop unrequired features
X_train = X_train.drop(["Similarity Score", "Filename", "Unified Quality Score", "Identification", "Face", "Under Exposure Prevention", "Over Exposure Prevention"], axis=1) # features
X_test = X_test.drop(["Similarity Score", "Filename", "Unified Quality Score", "Identification", "Face", "Under Exposure Prevention", "Over Exposure Prevention"], axis=1) # features

scaler = StandardScaler()
X_train = pd.DataFrame(scaler.fit_transform(X_train), columns=X_train.columns)
X_test = pd.DataFrame(scaler.transform(X_test), columns=X_test.columns)

# define a list of models to trial
models = [
    ("Logistic Regression", LogisticRegression()),
    ("Decision Tree", DecisionTreeClassifier()),
    ("Random Forest", RandomForestClassifier()),
    ("Gradient Boosting", GradientBoostingClassifier()),
    ("SVM", SVC(probability=True)),
    ("MLP", MLPClassifier(max_iter=1000)),
    ("Gaussian Naive Bayes", GaussianNB())
]

# initialize lists to store outputs
confusion_matrices = []
precision_scores = []
    
# train and evaluate each model
for name, model in models:
    print(f"Training: {name} model")
    model.fit(X_train, y_train)
    predictions = model.predict(X_test)
    
    # store confusion matrices
    cm = confusion_matrix(y_test, predictions)
    confusion_matrices.append([name, cm])
    
    # store precision scores
    precision = precision_score(y_test, predictions)
    precision_scores.append([name, precision])
    
    # store predictions and probabilities
    predictions_list = []
    probabilities = model.predict_proba(X_test)
    for actual, pred, prob, row in zip(y_test, predictions, probabilities, filenames_similarities.itertuples(index=False)):
        filename, similarity = row.Filename, row.Similarity_Score
        predictions_list.append([filename, actual, pred, max(prob), similarity])
    
    # write predictions and probabilities to CSV
    with open(os.path.join(evaluation_metrics_folder,f'{name}_predictions.csv'), 'w', newline='') as csvfile:
        writer = csv.writer(csvfile)
        writer.writerow(['Filename', 'Actual', 'Prediction', 'Probability', 'Similarity'])
        for row in predictions_list:
            writer.writerow(row)

# write confusion matrices to csv
with open(os.path.join(evaluation_metrics_folder, 'confusion_matrices.csv'), 'w', newline='') as csvfile:
    writer = csv.writer(csvfile)
    writer.writerow(['Model', 'Confusion Matrix'])
    for row in confusion_matrices:
        writer.writerow(row)

# write precision scores to csv
with open(os.path.join(evaluation_metrics_folder,'precision_scores.csv'), 'w', newline='') as csvfile:
    writer = csv.writer(csvfile)
    writer.writerow(['Model', 'precision'])
    for row in precision_scores:
        writer.writerow(row)

# calculate feature importance for each model and save to CSV

#initialise a list to store results
feature_importance_list = []

# Logistic Regression
coefficients = models[0][1].coef_[0]
features_df = pd.DataFrame({'Feature': X_train.columns, 'Importance': coefficients})
features_df['Abs_Coefficient'] = np.abs(features_df['Importance'])
features_df = features_df.sort_values('Abs_Coefficient', ascending=False)
feature_importance_list.append(['Logistic Regression', features_df[['Feature', 'Importance']]])

# Decision Tree
importance = models[1][1].feature_importances_
features_df = pd.DataFrame({'Feature': X_train.columns, 'Importance': importance})
features_df = features_df.sort_values('Importance', ascending=False)
feature_importance_list.append(['Decision Tree', features_df])

# Random Forest
importance = models[2][1].feature_importances_
features_df = pd.DataFrame({'Feature': X_train.columns, 'Importance': importance})
features_df = features_df.sort_values('Importance', ascending=False)
feature_importance_list.append(['Random Forest', features_df])

# Gradient Boosting
importance = models[3][1].feature_importances_
features_df = pd.DataFrame({'Feature': X_train.columns, 'Importance': importance})
features_df = features_df.sort_values('Importance', ascending=False)
feature_importance_list.append(['Gradient Boosting', features_df])

# save feature importance to CSV
for model_name, df in feature_importance_list:
    df.to_csv(os.path.join(evaluation_metrics_folder,f'{model_name}_feature_importance.csv'), index=False)

# optimise the Random Forest model
def RF_opt():  
    print('Optimising the Random Forest model')

    # initialize the Random Forest classifier
    rf = RandomForestClassifier()

    # define the parameter grid
    param_grid = {
        'n_estimators': [100, 200, 300, 500, 1000],
        'max_features': [1.0, 0.5, 'sqrt', 'log2'],
        'max_depth': [3, 4, 6, 8, 10, 12],
        'min_samples_split': [2, 10, 20],
        'min_samples_leaf': [1, 5, 10, 20],
        'criterion': ['gini', 'entropy'],
        'bootstrap': [True]
    }

    # initialize Grid Search
    grid_search = GridSearchCV(estimator=rf, param_grid=param_grid, cv=10, n_jobs=-1, verbose=2, scoring='precision')

    # fit the model
    grid_search.fit(X_train, y_train)

    # get the best parameters
    best_params = grid_search.best_params_
    print(f'Best parameters found: {best_params}')

    # predict using the best model
    best_rf = grid_search.best_estimator_
    y_pred = best_rf.predict(X_test)

    # evaluate the model
    precision = precision_score(y_test, y_pred)
    print(f'precision: {precision:.2f}')

    # write predictions and probabilities to CSV for the tuned Random Forest model
    predictions_list = []
    probabilities = best_rf.predict_proba(X_test)
    for actual, pred, prob, row in zip(y_test, y_pred, probabilities, filenames_similarities.itertuples(index=False)):
        filename, similarity = row.Filename, row.Similarity_Score
        predictions_list.append([filename, actual, pred, prob[1], similarity])

    with open(os.path.join(evaluation_metrics_folder, 'tuned_RF_predictions.csv'), 'w', newline='') as csvfile:
        writer = csv.writer(csvfile)
        writer.writerow(['Filename', 'Correct Identification', 'Prediction', 'Probability', 'Similarity'])
        for row in predictions_list:
            writer.writerow(row)

    # get feature importances
    feature_importances = best_rf.feature_importances_
    feature_names = X_train.columns

    # create a DataFrame for feature importances
    feature_importances_df = pd.DataFrame({
        'Feature': feature_names,
        'Importance': feature_importances
    })

    # sort the DataFrame by importance
    feature_importances_df = feature_importances_df.sort_values(by='Importance', ascending=False)

    # visualize feature importances
    plt.figure(figsize=(12, 8))
    plt.bar(feature_importances_df['Feature'], feature_importances_df['Importance'], align='center')
    plt.xticks(rotation=90)
    plt.xlabel('Feature')
    plt.ylabel('Importance')
    plt.title('Feature Importances')
    plt.tight_layout()
    plt.show() 

    # generate a learning curves for the best model
    train_sizes, train_scores, test_scores = learning_curve(
        estimator=best_rf,
        X=X_train,
        y=y_train,
        train_sizes=np.linspace(0.1, 1.0, 10),
        cv=5,
        scoring='precision'
    )

    # plot the learning curves
    plt.figure()
    plt.title("Learning Curves")
    plt.xlabel("Training examples")
    plt.ylabel("precision")

    train_mean = np.mean(train_scores, axis=1)
    train_std = np.std(train_scores, axis=1)
    test_mean = np.mean(test_scores, axis=1)
    test_std = np.std(test_scores, axis=1)

    plt.grid()

    plt.fill_between(train_sizes, train_mean - train_std,
                     train_mean + train_std, alpha=0.1,
                     color="r")
    plt.fill_between(train_sizes, test_mean - test_std,
                     test_mean + test_std, alpha=0.1,
                     color="g")
    plt.plot(train_sizes, train_mean, 'o-', color="r",
             label="Training score")
    plt.plot(train_sizes, test_mean, 'o-', color="g",
             label="Cross-validation score")

    plt.legend(loc="best")
    plt.show()
    
RF_opt()